﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountryAndCity.Classes
{
    class City
    {
        public City(Country country, string name, int population)
        {
            Name = name;
            Population = population;
            Country = country;
        }

        public string Name { get; set; }

        public int Population { get; set; }

        public Country Country { get; set; }
    }
}
