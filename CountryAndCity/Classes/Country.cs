﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountryAndCity.Classes
{
    class Country
    {
        public Country(string name)
        {
            Name = name;
        }
        public string Name { get; set; }

        private readonly List<City> _cities = new List<City>();
        public IEnumerable<City> Cities => _cities.AsEnumerable();

        public void AddCity(City city)
        {
            _cities.Add(city);
        }

    }
}
