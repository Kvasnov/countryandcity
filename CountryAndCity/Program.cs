﻿/*

Задание 3. Написать класс City и Country. 
Класс City должен иметь свойство типа Country (страна, в которой этот город), 
Country — свойство типа List<City> (города, которые в этой стране). 
City должен также иметь свойства Name и Population, Country — свойство Name.

3a. Вывести страны в порядке убывания суммарного населения (вывести название страны и суммарное население);
3b. Вывести страны в порядке возрастания населения самого крупного города (вывести название страны, название самого крупного города и население этого города);
3c. Вывести в случайном порядке все страны в формате Russia: Moscow, St. Petersburg, ... (то есть название страны, двоеточие и все её города через запятую в алфавитном порядке);
3d. Вывести в страны в порядке возрастания среднего по городам населения;
3e. Вывести пятёрку стран по количеству городов, но в алфавитном порядке;
ef. Вывести страны, в которых в самом крупном городе больше населения, чем суммарно в остальных.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CountryAndCity.Classes;

namespace CountryAndCity
{
    class Program
    {
        static void Main(string[] args)
        {
            var Russia = new Country("Russia");
            var city1 = new City (Russia, "Moscow", 1000 );
            var city2 = new City (Russia, "Omsk", 12 );

            Russia.AddCity(city1);
            Russia.AddCity(city2);

            var USA = new Country("USA");
            var city3 = new City(USA, "New-York", 2000);
            var city4 = new City(USA, "Miami", 120);

            USA.AddCity(city3);
            USA.AddCity(city4);

            var Portugal = new Country("Portugal");
            var city5 = new City(Portugal, "Porto", 1111);
            var city6 = new City(Portugal, "Lisboa", 2);
            var city7 = new City(Portugal, "Braga", 34);

            Portugal.AddCity(city5);
            Portugal.AddCity(city6);
            Portugal.AddCity(city7);

            var Ukrain = new Country("Ukrain");
            var city8 = new City(Ukrain, "Kiev", 11);

            Ukrain.AddCity(city8);

            var Belorus = new Country("Belorus");
            var city9 = new City(Belorus, "Minsk", 560);
            var city10 = new City(Belorus, "Gomel", 199);
            var city13 = new City(Belorus, "Gomel", 559);

            Belorus.AddCity(city9);
            Belorus.AddCity(city10);
            Belorus.AddCity(city13);

            var Spain = new Country("Spain");
            var city11 = new City(Spain, "Madrid", 890);
            var city12 = new City(Spain, "Sevilla", 567);

            Spain.AddCity(city11);
            Spain.AddCity(city12);


            var countries = new List<Country>();
            countries.Add(Russia);
            countries.Add(USA);
            countries.Add(Portugal);
            countries.Add(Ukrain);
            countries.Add(Belorus);
            countries.Add(Spain);

            //3a. Вывести страны в порядке убывания суммарного населения (вывести название страны и суммарное население);
            var task3a = countries
                .SelectMany(t => t.Cities)
                .GroupBy(t => t.Country.Name)
                .Select(t => new { Country = t.Key, Population = t.Sum(p => p.Population) })
                .OrderByDescending(t => t.Population)
                .ToList();
            
            foreach (var e in task3a)
            {
                Console.WriteLine("Население страны " + e.Country + " = " + e.Population);
            }

            Console.WriteLine();

            //3b.Вывести страны в порядке возрастания населения самого крупного города(вывести название страны, название самого крупного города и население этого города);
            //комментарии - Для каждой страны найти самый крупный по населению город, по этому значению отсортировать страны
            var task3b = countries
                .SelectMany(t => t.Cities)
                .GroupBy(t => t.Country.Name)
                .Select(t => new { Country = t.Key, Population = t.Max(p => p.Population), City = t.OrderByDescending(n => n.Population).Select(n => n.Name).First() })
                .OrderBy(t => t.Population)
                .ToList();

            foreach (var e in task3b)
            {
                Console.WriteLine("Страна - " + e.Country + ", самый крупный город в стране - " + e.City + ", население - " + e.Population);
            }

            Console.WriteLine();


            //3c.Вывести в случайном порядке все страны в формате Russia: Moscow, St.Petersburg, ... (то есть название страны, двоеточие и все её города через запятую в алфавитном порядке);
            var task3c = countries
                .SelectMany(t => t.Cities)
                .GroupBy(t => t.Country.Name)
                .Select(t => new { Country = t.Key, GroupCity = t.OrderBy(p => p.Name).Select(p => p.Name) })
                .ToList();

            foreach (var e in task3c)
            {
                Console.WriteLine("Страна - " + e.Country + " : " + string.Join(", ", e.GroupCity));
            }

            Console.WriteLine();


            //3d. Вывести в страны в порядке возрастания среднего по городам населения;
            var task3d = countries
                .SelectMany(t => t.Cities)
                .GroupBy(t => t.Country.Name)
                .Select(t => new { Country = t.Key, Population = t.Average(p => p.Population) })
                .OrderBy(t => t.Population).ToList();

            foreach (var e in task3d)
            {
                Console.WriteLine("Среднее население страны " + e.Country + " = " + Convert.ToInt32(e.Population));
            }

            Console.WriteLine();


            //3e. Вывести пятёрку стран по количеству городов, но в алфавитном порядке;
            //Комментарии - выбрать 5 стран с большим количеством городов и потом сортировать по алфавиту
            var task3e = countries
                .OrderBy(t => t.Cities.Count()).Reverse().Take(5)
                .SelectMany(t => t.Cities)
                .GroupBy(t => t.Country.Name)
                .Select(t => new { Country = t.Key, CountCity = t.Select(p => p.Name).Count()})
                .OrderBy(t => t.Country)
                .ToList();

            foreach (var e in task3e)
            {
                Console.WriteLine("В стране " + e.Country + " количество городов = " + e.CountCity); //
            }

            Console.WriteLine();

            // 3f.Вывести страны, в которых в самом крупном городе больше населения, чем суммарно в остальных.
            var task3f = countries
                .SelectMany(t => t.Cities)
                .GroupBy(t => t.Country.Name)
                .Select(t => new { Country = t.Key, MaxPopulationCity = t.Max(c => c.Population), OtherPopulation = t.Sum(p => p.Population) - t.Max(c => c.Population) }) //MaxPopulationCity = t.OrderBy(p => p.Population).Select(p => p.Name).Reverse().First()
                .OrderBy(t => t.Country)
                .ToList();

            foreach (var e in task3f)
            {
                if (e.MaxPopulationCity > e.OtherPopulation)
                {
                    Console.WriteLine("В стране " + e.Country + " самый населенный город = " + e.MaxPopulationCity); //
                }
            }


            Console.ReadKey();
        }
    }
}
